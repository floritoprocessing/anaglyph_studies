void setup() {
  size(720,576,P3D);
  initCubes();
}

void draw() {
  background(0); smooth(); noFill();
  updateCubes();
  translate(width/2.0,height/2.0);
  onScreenCubes();
  //saveFrame("c:/frames_3d/red_green_3d_2_####.tga");
}


Cube[] cube;
int nrOfCubes=3000;
float maxZ=200000;

void initCubes() {
  cube=new Cube[nrOfCubes];
  for (int i=0;i<nrOfCubes;i++) { cube[i]=new Cube(); cube[i].randomPos(50000); cube[i].randomRot(); cube[i].randomMove(); }
}
void updateCubes() { for (int i=0;i<nrOfCubes;i++) { cube[i].update(); } }
void onScreenCubes() { for (int i=0;i<nrOfCubes;i++) { cube[i].to3DScreen(); } }

class Cube {
  float zstart=0;
  float x=0,y=0,z=0;
  float zmov=0;
  float rx=1.3;
  float ry=0.0, ryAdd=0.06;
  float freq=0.1, pha=0.0;
  float side=250;
  Cube() {}
  
  void randomPos(float _r) {
    float aspRat=width/(float)height;
    zstart=-random(maxZ); z=zstart;
    x=random(-_r,_r); y=random(-_r/aspRat,_r/aspRat);
  }
  
  void randomRot() {
    rx=random(TWO_PI);
    ryAdd=random(0.03,0.08);
  }
  
  void randomMove() {
    zmov=random(150,600);
  }
  
  void update() {
    z+=zmov;
    if (z>100) {z-=maxZ;}
    ry+=ryAdd;
  }
  
  void to3DScreen() {
    pushMatrix();
    // left eye:
    //translate(50,0);
    pushMatrix();
    translate(-200,0);
    translate(x,y,z);
    rotateY(ry);
    rotateX(rx);
    stroke(0,255+255*(z/maxZ),0,128);
    box(side);
    popMatrix();  
  
    // right eye:
    //translate(-100,0);
    pushMatrix();
    translate(200,0);
    translate(x,y,z);
    rotateY(ry);
    rotateX(rx);
    stroke(255+255*(z/maxZ),0,0,128);
    box(side);
    popMatrix();
    popMatrix();
  }
}
