/**

IN THIS VERSION THE LEFT-EYE AND RIGHT-EYE IMAGES ARE CREATED THROUGH
ROTATING BOTH WORLDS INSTEAD OF TRANSLATING THEM

**/

PImage left, right, combined;
int fc=0; float ftime=0, lotime=1000000, hitime=0;
boolean end=false;

void setup() {
  size(720,576,P3D);
  left=loadImage("720x576.jpg");
  right=loadImage("720x576.jpg");
  combined=loadImage("720x576.jpg");
  initCubes();
}

void draw() {
  smooth();
  updateCubes(end);
  
  pushMatrix();
  createLeft(left);
  createRight(right);
  popMatrix();
  
  createAnaglyph(left,right,combined);
  
  background(0);
  noTint();
  image(combined,0,0);
  
  //saveFrame("c:/frames_3d/red_green_3d_04_1_####.bmp");
  float frametime=millis()-ftime;
  if (frametime<lotime) {lotime=frametime;} if (frametime>hitime) {hitime=frametime;}
  //println("frame "+fc+" saved. (frametime: "+frametime+" ms - lo/hi: "+lotime+"/"+hitime+" ms)"); 
  ftime=millis();
  fc++; if (fc>1000) {end=true;}
}



Cube[] cube;
int nrOfCubes=5000;
float maxZ=100000;

void initCubes() {
  cube=new Cube[nrOfCubes];
  for (int i=0;i<nrOfCubes;i++) { cube[i]=new Cube(); cube[i].randomPos(60000); cube[i].randomRot(); cube[i].randomMove(); }
}

void updateCubes(boolean _end) { for (int i=0;i<nrOfCubes;i++) { cube[i].update(_end); } }

void createLeft(PImage bi) {
  colorMode(RGB,255); background(255); 
//  translate(-55,0,0);
  for (int i=0;i<nrOfCubes;i++) { 
    cube[i].toScreenLeft();
  }
  loadPixels();
  for (int i=0;i<bi.pixels.length;i++) {bi.pixels[i]=pixels[i];}
}

void createRight(PImage bi) {
  colorMode(RGB,255); background(255); 
//  translate(110,0,0);
  for (int i=0;i<nrOfCubes;i++) { 
    cube[i].toScreenRight();
  }
  for (int i=0;i<bi.pixels.length;i++) {bi.pixels[i]=pixels[i];}
}


class Cube {
  float zstart=0;
  float x=0,y=0,z=0;
  float zmov=0;
  float rx=1.3, rxAdd=0.06;
  float ry=0.0, ryAdd=0.06;
  float freq=0.1, pha=0.0;
  float side1,side2,side3;
  boolean active=true;
  boolean visible=false;
  Cube() {
    side1=random(20,800);
    side2=random(20,800);
    side3=random(20,800);
  }
  
  void randomPos(float _r) {
    float aspRat=(float)width/(float)height;
    zstart=-random(maxZ); z=zstart;
    x=random(-_r,_r); y=random(-_r/aspRat,_r/aspRat);
  }
  
  void randomRot() {
    rx=random(TWO_PI);
    ry=random(TWO_PI);
    rxAdd=2*random(0.04,0.10);
    ryAdd=2*random(0.04,0.10);
  }
  
  void randomMove() {
    zmov=random(100,300);
  }
  
  void update(boolean _end) {
    if (active) {
      rx+=rxAdd; ry+=ryAdd;
      z+=zmov;
      if (z>-300) {
        if (!_end) {z-=maxZ;visible=true;} else {active=false;visible=false;}
      }
    }
  }
  
  void toScreenLeft() {// left eye:
    if (visible) {
    pushMatrix();
    float bright=1+1*(z/maxZ);
    float c1=255-255*bright;
    float c2=255-64*bright;
    stroke(c1,c1,c1);
    fill(c2,c2,c2);
//    translate(100,0);
    translate(0,0,maxZ/2.0);
    rotateY(radians(-0.2));
    translate(0,0,-maxZ/2.0);
    translate(x,y,z);
    rotateY(ry);
    rotateX(rx);
    box(side1,side2,side3);
    popMatrix(); 
    }
  }
  
  void toScreenRight() {// right eye:
    if (visible) {
    pushMatrix();
    float bright=1+1*(z/maxZ);
    float c1=255-255*bright;
    float c2=255-64*bright;
    stroke(c1,c1,c1);
    fill(c2,c2,c2);
    translate(0,0,maxZ/2.0);
    rotateY(radians(0.2));
    translate(0,0,-maxZ/2.0);
//    translate(-100,0);
    translate(x,y,z);
    rotateY(ry);
    rotateX(rx);
    box(side1,side2,side3);
    popMatrix();
    }
  }
  
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// ANAGLYPH IMAGE CREATION ////////////////////////////////////////

////////// red/green PImage creation

void createAnaglyph(PImage img1, PImage img2, PImage img) {
  createRed(img1);
  createGreen(img2);
  screen(img1,img2,img);
}

// create red channel: (uses image buffer);
PImage createRed(PImage img) {
  background(0); colorMode(HSB,255); tint(0,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// create green channel: (uses image buffer);
PImage createGreen(PImage img) {
  background(0); colorMode(HSB,255); tint(90,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// screen two images:
PImage screen(PImage img_a, PImage img_b, PImage img) {
  int a,b;
  for (int i=0;i<img_a.pixels.length;i++) {
    a=img_a.pixels[i]; b=img_b.pixels[i];
    int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
    int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
    int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
    img.pixels[i]=(255<<24|rr<<16|gg<<8|bb);
  }
  return img;
}

// color functions:
int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

//////////////////////////////////////// end ANAGLYPH IMAGE CREATION ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
