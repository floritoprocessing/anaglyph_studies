/**

IN THIS VERSION THE LEFT-EYE AND RIGHT-EYE IMAGES ARE CREATED THROUGH
ROTATING BOTH WORLDS INSTEAD OF TRANSLATING THEM

**/

PImage left, right, combined;
boolean anaglyph=false;
int fc=0;

void setup() {
  size(720,576,P3D); smooth();
  left=loadImage("720x576.jpg");
  right=loadImage("720x576.jpg");
  combined=loadImage("720x576.jpg");
  initObjects();
}

void draw() {
  updateObjects();

  if (anaglyph) {  
    objectsToImage(left,0);
    objectsToImage(right,1);
    createAnaglyph(left,right,combined);
  } else {
    objectsToImage(combined,2);
  }
  
  background(0); noTint(); image(combined,0,0); //saveFrame("c:/frames_3d/red_green_3d_05_01_####.tga"); print("f: "+fc+" ");
  fc++;
}

void mousePressed() {anaglyph=!anaglyph;}





//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// class Object ////////////////////////////////////////

Object[] object;
int nrOfObjects=7000;
float maxX=3000, maxY=3000, maxZ=100000, minZ=-300;
float mainDepth=0.15;  // value 0..1 for z-depth of stereo image

void initObjects() {
  object=new Object[nrOfObjects];
  for (int i=0;i<nrOfObjects;i++) { 
    object[i]=new Object(); 
    object[i].randomPos(maxX,maxY); 
    object[i].randomRot(); 
  }
}

void updateObjects() { 
  for (int i=0;i<nrOfObjects;i++) { 
    object[i].pos.add(0,0,100);
    object[i].pos.add(30*sin(0.1*TWO_PI*fc/25.0),0,0);
    object[i].pos.rotZ(-radians(0.2));
    object[i].updateBodyRotation(); 
    object[i].updateClipping();
  } 
}

void objectsToImage(PImage bi, int _mode) {
  // mode 0..2: left eye, right eye, center
  colorMode(RGB,255);background(255);
  loadPixels();
  for (int i=0;i<nrOfObjects;i++) { object[i].toScreen(_mode,mainDepth); }
  for (int i=0;i<bi.pixels.length;i++) {bi.pixels[i]=pixels[i];}
}

class Object {
  Vec pos=new Vec(0,0,0);
  float rx=0, rxAdd=0;
  float ry=0, ryAdd=0;
  Shape shape;
  boolean visible=true;

  Object() { shape=new Shape((int)random(3,5)); shape.randomVertexes(random(20,150)); }
  
  void randomPos(float _x, float _y) { pos=new Vec(random(-_x,_x),random(-_y,_y),minZ-random(maxZ)); }
  void randomRot() {
    rx=random(TWO_PI); rxAdd=1*random(0.04,0.10);
    ry=random(TWO_PI); ryAdd=1*random(0.04,0.10);
  }
 
  void updateBodyRotation() { rx+=rxAdd; ry+=ryAdd; }
  
  void updateClipping() {
    if (pos.x>maxX) {pos.x-=(2*maxX);} if (pos.x<-maxX) {pos.x+=(2*maxX);}
    if (pos.y>maxY) {pos.y-=(2*maxY);} if (pos.y<-maxY) {pos.y+=(2*maxY);}
    if (pos.z>minZ) {pos.z-=maxZ;}
  }
  
  void toScreen(int mode,float _depth) {  // mode 0..2: left eye, right eye, center
    pushMatrix();
    float bright=1+1*(pos.z/maxZ);
    float c1=255-255*bright;
    float c2=255-64*bright;
    stroke(c1,c1,c1);
    fill(c2,c2,c2);
    if (mode!=2) {
      translate(0,0,maxZ*_depth);
      rotateY(radians(mode==0?-0.2:0.2));
      translate(0,0,-maxZ*_depth);
    }
    translate(pos.x,pos.y,pos.z);
    rotateY(ry);
    rotateX(rx);
    shape.toScreen();
    popMatrix();
  }
}

//////////////////////////////////////// end class Object ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////




class Shape {
  int nrOfVertexes=2;
  Vec[] pos;
  
  Shape() { pos=new Vec[nrOfVertexes]; }
  Shape(int _nr) { nrOfVertexes=_nr; pos=new Vec[nrOfVertexes]; }
  
  void randomVertexes(float _r) {
    Vec v=new Vec(random(-_r,_r),random(-_r,_r),random(-_r,_r));
    for (int i=0;i<nrOfVertexes;i++) {
      Vec accum=new Vec(random(-_r,_r),random(-_r,_r),random(-_r,_r));
      v.add(accum);
      pos[i]=new Vec(v);
    }
  }
  
  void toScreen() {
    beginShape(POLYGON);
    for (int i=0;i<nrOfVertexes;i++) { vertex(pos[i].x,pos[i].y,pos[i].z); }
    endShape();
  }
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// ANAGLYPH IMAGE CREATION ////////////////////////////////////////

////////// red/green PImage creation
void createAnaglyph(PImage img1, PImage img2, PImage img) {
  createRed(img1);
  createGreen(img2);
  screen(img1,img2,img);
}

// create red channel: (uses image buffer);
PImage createRed(PImage img) {
  background(0); colorMode(HSB,255); tint(0,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// create green channel: (uses image buffer);
PImage createGreen(PImage img) {
  background(0); colorMode(HSB,255); tint(90,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// screen two images:
PImage screen(PImage img_a, PImage img_b, PImage img) {
  int a,b;
  for (int i=0;i<img_a.pixels.length;i++) {
    a=img_a.pixels[i]; b=img_b.pixels[i];
    int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
    int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
    int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
    img.pixels[i]=(rr<<16|gg<<8|bb);
  }
  return img;
}

// color functions:
int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

//////////////////////////////////////// end ANAGLYPH IMAGE CREATION ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////





///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// Vec class ////////////////////////////////////////

class Vec {
  float x=0,y=0,z=0;
  Vec() {}
  Vec(float _x, float _y) {x=_x;y=_y;}
  Vec(float _x, float _y, float _z) {x=_x;y=_y;z=_z;}
  Vec(Vec v) {x=v.x;y=v.y;z=v.z;}
  
  void add(Vec v) {x+=v.x;y+=v.y;z+=v.z;}
  void add(float _x, float _y) {x+=_x;y+=_y;}
  void add(float _x, float _y, float _z) {x+=_x;y+=_y;z+=_z;}
  
  void sub(Vec v) {x-=v.x;y-=v.y;z-=v.z;}
  void sub(float _x, float _y) {x-=_x;y-=_y;}
  void sub(float _x, float _y, float _z) {x-=_x;y-=_y;z-=_z;}
  
  void mul(float n) {x*=n;y*=n;z*=n;}
  void div(float n) {x/=n;y/=n;z/=n;}
  float magnitude() { return sqrt(x*x+y*y+z*z); }
  // rotation around zero vector
  void rotX(float rd) { float SI=sin(rd), CO=cos(rd); float ny=y*CO-z*SI, nz=z*CO+y*SI; y=ny; z=nz; }
  void rotY(float rd) { float SI=sin(rd), CO=cos(rd); float nx=x*CO-z*SI, nz=z*CO+x*SI; x=nx; z=nz; }
  void rotZ(float rd) { float SI=sin(rd), CO=cos(rd); float nx=x*CO-y*SI, ny=y*CO+x*SI; x=nx; y=ny; }
  // rotation around a fixed point
  void rotX(float rd, Vec v) { sub(v); rotX(rd); add(v); }
  void rotY(float rd, Vec v) { sub(v); rotY(rd); add(v); }
  void rotZ(float rd, Vec v) { sub(v); rotZ(rd); add(v); }
}

Vec add(Vec v, Vec w) { return (new Vec(v.x+w.x,v.y+w.y,v.z+w.z)); }
Vec sub(Vec v, Vec w) { return (new Vec(v.x-w.x,v.y-w.y,v.z-w.z)); }
Vec mul(Vec v, float n) { return (new Vec(v.x*n,v.y*n,v.z*n)); }
Vec div(Vec v, float n) { return (new Vec(v.x/n,v.y/n,v.z/n)); }

//////////////////////////////////////// end Vec class ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
