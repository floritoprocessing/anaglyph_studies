// red green research

PImage left,right,combined;

int mode=0; boolean clicked;

void setup() {
  size(310,246,P3D);
  background(0);

  left=loadImage("sample_left.jpg");
  right=loadImage("sample_right.jpg");
  combined=loadImage("sample_left.jpg");

  // create red and green color images:
  
  float nu=millis();
  createAnaglyph(left,right,combined);
  println("calc time: "+(millis()-nu)+" ms");

  
  colorMode(RGB,255); noTint(); background(0);
  image(combined,0,0);
  
  save("red_green_research.jpg");
}






/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// ANAGLYPH IMAGE CREATION ////////////////////////////////////////

////////// red/green BImage creation

void createAnaglyph(PImage img1, PImage img2, PImage img) {
  createRed(img1);
  createGreen(img2);
  screen(img1,img2,img);
}

// create red channel: (uses image buffer);
PImage createRed(PImage img) {
  background(0); 
  colorMode(HSB,255); 
  tint(0,255,255); 
  image(img,0,0);
  img.loadPixels();
  loadPixels();
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// create green channel: (uses image buffer);
PImage createGreen(PImage img) {
  background(0); colorMode(HSB,255); tint(90,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// screen two images:
PImage screen(PImage img_a, PImage img_b, PImage img) {
  int a,b;
  for (int i=0;i<img_a.pixels.length;i++) {
    a=img_a.pixels[i]; b=img_b.pixels[i];
    int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
    int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
    int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
    img.pixels[i]=(rr<<16|gg<<8|bb);
  }
  return img;
}

// color functions:
int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

//////////////////////////////////////// end ANAGLYPH IMAGE CREATION ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
