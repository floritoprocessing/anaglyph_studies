PImage left, right, combined;

void setup() {
  size(720,576,P3D);
  left=loadImage("720x576.jpg");
  right=loadImage("720x576.jpg");
  combined=loadImage("720x576.jpg");
  initCubes();
}

void draw() {
  smooth(); noFill();
  updateCubes();
  
  pushMatrix();
  createLeft(left);
  createRight(right);
  popMatrix();
  
  createAnaglyph(left,right,combined);
  
  background(0);
  noTint();
  image(combined,0,0);
  
//  saveFrame("c:/frames_3d/red_green_3d_03_###.tga");
}


Cube[] cube;
int nrOfCubes=1000;
float maxZ=50000;

void initCubes() {
  cube=new Cube[nrOfCubes];
  for (int i=0;i<nrOfCubes;i++) { cube[i]=new Cube(); cube[i].randomPos(10000); cube[i].randomRot(); cube[i].randomMove(); }
}

void updateCubes() { for (int i=0;i<nrOfCubes;i++) { cube[i].update(); } }

void createLeft(PImage bi) {
  background(0); colorMode(RGB,255);
  translate(-55,0,0);
  for (int i=0;i<nrOfCubes;i++) { 
    cube[i].toScreenLeft();
  }
  loadPixels();
  for (int i=0;i<bi.pixels.length;i++) {bi.pixels[i]=pixels[i];}
}

void createRight(PImage bi) {
  background(0); colorMode(RGB,255);
  translate(110,0,0);
  for (int i=0;i<nrOfCubes;i++) { 
    cube[i].toScreenRight();
  }
  for (int i=0;i<bi.pixels.length;i++) {bi.pixels[i]=pixels[i];}
}


class Cube {
  float zstart=0;
  float x=0,y=0,z=0;
  float zmov=0;
  float rx=1.3;
  float ry=0.0, ryAdd=0.06;
  float freq=0.1, pha=0.0;
  float side=250;
  Cube() {}
  
  void randomPos(float _r) {
    float aspRat=(float)width/(float)height;
    zstart=-random(maxZ); z=zstart;
    x=random(-_r,_r); y=random(-_r/aspRat,_r/aspRat);
  }
  
  void randomRot() {
    rx=random(TWO_PI);
    ryAdd=random(0.03,0.08);
  }
  
  void randomMove() {
    zmov=random(100,400);
  }
  
  void update() {
    z+=zmov; if (z>100) {z-=maxZ;}
    ry+=ryAdd;
  }
  
  void toScreenLeft() {// left eye:
    pushMatrix();
    float bright=1+1*(z/maxZ);
    float c1=255*bright;
    float c2=192*bright;
    stroke(c1,c1,c1);
    fill(c2,c2,c2);
    translate(100,0);
    translate(x,y,z);
    rotateY(ry);
    rotateX(rx);
    box(side);
    popMatrix(); 
  }
  
  void toScreenRight() {// right eye:
    pushMatrix();
    float bright=1+1*(z/maxZ);
    float c1=255*bright;
    float c2=192*bright;
    stroke(c1,c1,c1);
    fill(c2,c2,c2);
    translate(-100,0);
    translate(x,y,z);
    rotateY(ry);
    rotateX(rx);
    box(side);
    popMatrix();
  }
  
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// ANAGLYPH IMAGE CREATION ////////////////////////////////////////

////////// red/green PImage creation

void createAnaglyph(PImage img1, PImage img2, PImage img) {
  createRed(img1);
  createGreen(img2);
  screen(img1,img2,img);
}

// create red channel: (uses image buffer);
PImage createRed(PImage img) {
  background(0); colorMode(HSB,255); tint(0,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// create green channel: (uses image buffer);
PImage createGreen(PImage img) {
  background(0); colorMode(HSB,255); tint(90,255,255); image(img,0,0);
  for (int i=0;i<img.pixels.length;i++) { img.pixels[i]=pixels[i]; }
  return img;
}

// screen two images:
PImage screen(PImage img_a, PImage img_b, PImage img) {
  int a,b;
  for (int i=0;i<img_a.pixels.length;i++) {
    a=img_a.pixels[i]; b=img_b.pixels[i];
    int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
    int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
    int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
    img.pixels[i]=(rr<<16|gg<<8|bb);
  }
  return img;
}

// color functions:
int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

//////////////////////////////////////// end ANAGLYPH IMAGE CREATION ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
