AnaglyphImage image3D;
Structure3D struct;

// 3D glasses setup:
// left eye: RED
// right eye: BLUE or GREEN

float eyeDistance=10;

void setup() {
  size(700,400,P3D);
  image3D = new AnaglyphImage(width,height);
  struct = new Structure3D();
  println("[space] to reset");
  println("[-] to decrease eye distance");
  println("[+] to increase eye distance");
}

void keyPressed() {
  if (key==' ') setup();
  if (key=='+'&&eyeDistance<30) eyeDistance+=0.2;
  if (key=='-'&&eyeDistance>1) eyeDistance-=0.2;
}

void draw() {
  
  struct.morph();
  
  background(64,64,64);
  struct.draw(eyeDistance);
  image3D.captureLeft();
  
  background(64,64,64);
  struct.draw(-eyeDistance);
  image3D.captureRight();
  
  background(64,64,64);
  image(image3D.getImage(),0,0);
  stroke(255,255,255);
  line(width/2.0-eyeDistance,height,width/2.0-eyeDistance,height-20);
  line(width/2.0+eyeDistance,height,width/2.0+eyeDistance,height-20);
  line(width/2.0-eyeDistance,height-10,width/2.0+eyeDistance,height-10);
  
  //println();
  //saveFrame("D:\\Processing visual index\\tmp\\"+getClass().getName()+"_####.bmp");
}
