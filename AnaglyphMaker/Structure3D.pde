class Structure3D {

  private int OBJECT_AMOUNT = 2500;
  private int OBJECT_AMOUNT_SOLID = 200;

  private float[][] pos, mov;
  private float[][] siz;
  private float[][] rot, rom;

  private float worldBumpX, worldBumpY;
  private float xFreq, yFreq;

  private float LIMIT_Z = 0;
  private float LIMIT_Z_FAR = 10000;

  Structure3D() {
    pos = new float[OBJECT_AMOUNT][3];
    mov = new float[OBJECT_AMOUNT][3];
    siz = new float[OBJECT_AMOUNT][3];
    rot = new float[OBJECT_AMOUNT][3];
    rom = new float[OBJECT_AMOUNT][3];
    for (int i=0;i<OBJECT_AMOUNT;i++) {
      for (int v=0;v<3;v++) {
        pos[i][v]=(v==0)?random(-2*width,2*width):((v==1)?random(-2*height,2*height):-random(LIMIT_Z_FAR));
        mov[i][v]=(v<2)?random(-0.2,0.2):random(10.0,50.0);
        siz[i][v]=random(width/800.0,width/50.0);
        rot[i][v]=random(TWO_PI);
        rom[i][v]=random(-0.06,0.06);
      }
    }

    xFreq = random(0.1,1.0);
    yFreq = random(0.1,1.0);
  }

  void morph() {
    worldBumpX=200*sin(xFreq*frameCount/20.0);
    worldBumpY=200*sin(yFreq*frameCount/20.0);
    for (int i=0;i<OBJECT_AMOUNT;i++) {
      for (int v=0;v<3;v++) {
        pos[i][v]+=mov[i][v];
        rot[i][v]+=rom[i][v];
      }
      if (pos[i][2]>LIMIT_Z) pos[i][2]-=LIMIT_Z_FAR;
    }
  }

  void draw(float offX) {
    ellipseMode(CENTER);
    pushMatrix();
    translate(width/2.0,height/2.0);
    for (int i=0;i<OBJECT_AMOUNT;i++) {
      stroke(224,224,224,255-255*-pos[i][2]/LIMIT_Z_FAR);

      if (i<OBJECT_AMOUNT_SOLID) {
        fill(224,224,224,255-255*-pos[i][2]/LIMIT_Z_FAR);
        pushMatrix();
        translate(pos[i][0]+offX,pos[i][1],pos[i][2]);
        rotateX(rot[i][0]);
        rotateY(rot[i][1]);
        ellipse(0,0,siz[i][0],siz[i][1]);
        popMatrix();
      } 
      else {
        point(pos[i][0]+offX+worldBumpX,pos[i][1]+worldBumpY,pos[i][2]);
      }
    }
    popMatrix();
  }

}
