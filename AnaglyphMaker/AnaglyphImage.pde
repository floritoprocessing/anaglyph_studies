class AnaglyphImage {
  
  private PImage img;
  private int width, height;
  
  AnaglyphImage(int w, int h) {
    img = new PImage(w,h);
    for (int i=0;i<img.pixels.length;i++) img.pixels[i]=0x000000;
    width = w;
    height = h;
  }
 
  void captureLeft() {
    loadPixels();
    for (int i=0;i<pixels.length;i++) {
      int b=((pixels[i]>>16&0xFF) + (pixels[i]>>8&0xFF) + (pixels[i]&0xFF))/3;
      img.pixels[i]=color(b,0,0);
    }
  }
  
  void captureRight() {
    loadPixels();
    for (int i=0;i<pixels.length;i++) {
      int b=((pixels[i]>>16&0xFF) + (pixels[i]>>8&0xFF) + (pixels[i]&0xFF))/3;
      img.pixels[i]=color(img.pixels[i]>>16&0xFF,b,b);
    }
  }
  
  PImage getImage() {
    return img;
  }
}
